Descrição da Atividade:
Nesta atividade utilizaremos dois arquivos:
1) WAGE1.DES – Descreve brevemente o conteúdo de cada coluna do arquivo de
dados. São dados acerca, principalmente, sobre salários, nível de instrução,
experiência e tempo de atuação. Mas são complementados com uma série de outras
informações (sexo, local de trabalho, setor, número de dependentes, etc.)
2) WAGE1.raw – Arquivo de dados (526 registros, coletados em 1976).
Originalmente, essa atividade era livre. No sentido, que cada um deveria explorar e
analisar o arquivo e tirar suas próprias conclusões. No entanto, fiz algumas alterações,
introduzindo uma espécie de roteiro de trabalho e deixando a parte final livre para suas
conclusões.
Todas as análises práticas deverão ser feitas utilizando o software R, sempre que possível.
Roteiro sugerido:
1) Realizar uma análise descritiva geral, diferenciando com o auxílio de gráficos, salários
de homens e mulheres. Se são casados ou não. Existe alguma influência da questão racial
(variável nonwhite)? Teste essas diferenças. São significativas (Teste de Hipóteses)?
2) Aprofunde o estudo anterior. Observe se o local de trabalho e o ramo de atividades
gera diferenças. Teste essas diferenças. São significativas (Teste de Hipóteses)?
3) Como o salário (por hora) relaciona-se com os anos de estudo? Com os anos de
experiência? Com os anos na mesma empresa? Existe alguma relação? Diferencie por
sexo, região, casado ou não, etc. Você pode utilizar gráficos de dispersão e linhas de
tendência ou fazer um teste de regressão.
4) As três últimas colunas nos dão o logaritmo do salário, o quadrado do número de anos
de experiência e o quadrado do número de anos na empresa atual, respectivamente. Esses
dados podem ser utilizados para alguma análise? Reflita, pesquise, faça testes, etc.

5) O que mais é possível fazer? Esse é o seu momento de propor alguma análise e testá-
la. Se você achar que é pertinente.

6) Conclusões – elabore um relatório descritivo (uma página) sobre suas conclusões
acerca deste conjunto de dados. Que informações você obteve? Qual a validade estatística
dessas conclusões? Qual o nível de confiabilidade?
BOM TRABALHO!